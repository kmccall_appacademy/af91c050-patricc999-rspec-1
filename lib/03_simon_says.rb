def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, times = 2)
  result = (string + ' ') * times
  result.rstrip
end

def start_of_word(word, num_letters)
  word[0..num_letters - 1]
end

def first_word(string)
  string.split(' ')[0]
end

def titleize(string)
  words = string.split(' ')
  first_word = words.shift.capitalize
  capital_words = words.map do |word|
    if !is_little_word?(word)
      word.capitalize
    else
      word
    end
  end
  result = first_word + ' ' + capital_words.join(' ')
  result.rstrip
end

def is_little_word?(word)
  little_words = ['the', 'over', 'and', 'of']
  little_words.include?(word)
end
