def translate(sentance)
  translated_sentance = []
  words = sentance.split(' ')
  words.each do |word|
    if is_vowel?(word[0]) && word[0..1] != 'qu'
      translated_sentance.push(word + 'ay')
    else
      translated_word = translate_word(word)
      translated_sentance.push(translated_word)
    end
  end
  translated_sentance.join(' ')
end

def is_vowel?(letter)
  vowels = ['a', 'e', 'i', 'o', 'u']
  vowels.include?(letter)
end

def find_first_vowel(word)
  word.chars.each.with_index do |letter, idx|
    if is_vowel?(letter) && word[idx - 1] + letter != 'qu'
      return idx
    end
  end
end

def translate_word(word)
  first_vowel_index = find_first_vowel(word)
  word[first_vowel_index..word.length] + word[0...first_vowel_index] + 'ay'
end
